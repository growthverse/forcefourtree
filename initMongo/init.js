/**
 * Created by celine on 08/11/15.
 */
conn = new Mongo();
db = conn.getDB("forcefourtree");

//Clean collection forcefourtree
db.forcefourtree.remove({});

//Save categories
db.forcefourtree.save(
    { "_id" : ObjectId("563f016944ae94ab40c0962f"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Genre", "type" : "category", "keywords" : [ "Action", "Aventure", "Comedie", "Drame" ] }
);
db.forcefourtree.save(
{ "_id" : ObjectId("563f017f44ae94ab40c09630"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Public", "type" : "category", "keywords" : [ "Adulte", "Enfant", "Ado" ] }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f01a444ae94ab40c09631"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Continent", "type" : "category", "keywords" : [ "Amerique", "Asie", "Europe", "Afrique", "Australie" ] }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f01cb44ae94ab40c09632"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Arrete moi si tu peux", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Action", "Aventure" ], "563f017f44ae94ab40c09630" : [ "Adulte", "Ado" ], "563f01a444ae94ab40c09631" : [ "Amerique" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f01d944ae94ab40c09633"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Cendrillon", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Drame" ], "563f017f44ae94ab40c09630" : [ "Enfant" ], "563f01a444ae94ab40c09631" : [ "Europe" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f022f44ae94ab40c09634"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Inspecteur La Bavure", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Comedie" ], "563f017f44ae94ab40c09630" : [ "Adulte" ], "563f01a444ae94ab40c09631" : [ "Europe" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f02aa44ae94ab40c09635"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Seul sur Mars", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "false", "Aventure" ], "563f017f44ae94ab40c09630" : [ "Adulte", "Ado" ], "563f01a444ae94ab40c09631" : [ "Asie" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f02ba44ae94ab40c09636"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Aladin", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Aventure" ], "563f017f44ae94ab40c09630" : [ "Enfant" ], "563f01a444ae94ab40c09631" : [ "Afrique" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f02cd44ae94ab40c09637"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Kirikou", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Aventure" ], "563f017f44ae94ab40c09630" : [ "Enfant" ], "563f01a444ae94ab40c09631" : [ "Australie" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f02ea44ae94ab40c09638"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Peter Pan", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Aventure" ], "563f01a444ae94ab40c09631" : [ "Australie" ], "563f017f44ae94ab40c09630" : [ "Enfant" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f031144ae94ab40c09639"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Forest Gump", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Comedie", "Drame" ], "563f017f44ae94ab40c09630" : [ "Adulte" ], "563f01a444ae94ab40c09631" : [ "Europe" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f032644ae94ab40c0963a"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "La ligne verte", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Drame" ], "563f017f44ae94ab40c09630" : [ "Adulte" ], "563f01a444ae94ab40c09631" : [ "Amerique" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f033344ae94ab40c0963b"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Shrek", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Comedie" ], "563f017f44ae94ab40c09630" : [ "Adulte", "Enfant", "Ado" ], "563f01a444ae94ab40c09631" : [ "Amerique" ] } }
);
db.forcefourtree.save(
    { "_id" : ObjectId("563f035f44ae94ab40c0963c"), "_class" : "com.univ.smb.forceFourTree.models.Node", "name" : "Pulp Fiction", "type" : "leaf", "facets" : { "563f016944ae94ab40c0962f" : [ "Action" ], "563f017f44ae94ab40c09630" : [ "Adulte" ], "563f01a444ae94ab40c09631" : [ "Asie" ] } }
);

cursor = db.forcefourtree.find();
while ( cursor.hasNext() ) {
    printjson(cursor.next());
}


db.users.remove({});

db.users.save(
    { "_id" : ObjectId("563f109a44aed872d5659d1d"), "_class" : "com.univ.smb.forceFourTree.models.User", "name" : "Celine", "password" : "debil", "role" : "committer" }
);
db.users.save(
    { "_id" : ObjectId("563f10a144aed872d5659d1e"), "_class" : "com.univ.smb.forceFourTree.models.User", "name" : "Claude", "password" : "debil", "role" : "committer" }
);
db.users.save(
    { "_id" : ObjectId("563f10ae44aed872d5659d1f"), "_class" : "com.univ.smb.forceFourTree.models.User", "name" : "Marcel", "password" : "debil", "role" : "moderator" }
);
db.users.save(
    { "_id" : ObjectId("563f10b744aed872d5659d20"), "_class" : "com.univ.smb.forceFourTree.models.User", "name" : "Maud", "password" : "debil", "role" : "moderator" }
);
db.users.save(
    { "_id" : ObjectId("563f10bf44aed872d5659d21"), "_class" : "com.univ.smb.forceFourTree.models.User", "name" : "Alfred", "password" : "debil", "role" : "admin" }
);
db.users.save(
    { "_id" : ObjectId("563f10c444aed872d5659d22"), "_class" : "com.univ.smb.forceFourTree.models.User", "name" : "Attila", "password" : "debil", "role" : "admin" }
);

cursor = db.users.find();
while ( cursor.hasNext() ) {
    printjson(cursor.next());
}





