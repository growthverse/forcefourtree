(function () {
    "use strict";

    var module = angular.module("services");

    module.factory("userService", ["$q", "Restangular", function ($q, Restangular) {
        var UserService = {};

        UserService.getAllUsers = function (page) {
            var data = $q.defer();
            var pageRequest = 'page=' + (page ? page : 0);

            Restangular
                .one('user')
                .one('getAllUsers.json?' + pageRequest)
                .get()
                .then(function(response) {
                    data.resolve(response.page);
                });

            return data.promise;
        };

        UserService.add = function (user) {
            return Restangular
                .one('user')
                .post('create.json', user);
        };

        UserService.update = function (user) {
            return Restangular
                .one('user')
                .one('update', user.id + '.json')
                .customPUT(user);
        };

        UserService.delete = function (user) {
            return Restangular
                .one('user')
                .one('delete', user.id + '.json')
                .remove();
        };
        
        return UserService;
    }]);

})();