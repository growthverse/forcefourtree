(function () {
    "use strict";

    var module = angular.module("services");

    module.factory("nodeService", ["$q", "Restangular", function ($q, Restangular) {
        var NodeService = {};

        NodeService.getAllNodes = function (page) {
            var data = $q.defer();
            var pageRequest = 'page=' + (page ? page : 0);

            Restangular
                .one('node')
                .one('getAllNodes.json?' + pageRequest)
                .get()
                .then(function(response) {
                    data.resolve(response.page);
                });

            return data.promise;
        };

        NodeService.add = function (node) {
            return Restangular
                .one('node')
                .post('create.json', node);
        };

        NodeService.update = function (node) {
            return Restangular
                .one('node')
                .one('update', node.id + '.json')
                .customPUT(node);
        };

        NodeService.delete = function (node) {
            return Restangular
                .one('node')
                .one('delete', node.id + '.json')
                .remove();
        };

        NodeService.getAllCategories = function (node) {
            var data = $q.defer();

            Restangular
                .one('node')
                .one('getAllCategories.json')
                .get()
                .then(function(response) {
                    data.resolve(response.nodeList);
                });

            return data.promise;
        };

        return NodeService;
    }]);

})();