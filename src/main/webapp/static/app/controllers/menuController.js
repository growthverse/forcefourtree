(function () {
    "use strict";

    var module = angular.module('controllers');

    module.controller('menuController', ["$rootScope", "$scope", "$location", function($rootScope, $scope, $location) {
        $scope.isActive = function (route) {
            return route == $location.path();
        };

    }]);
})();