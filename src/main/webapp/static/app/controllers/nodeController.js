(function () {
    "use strict";

    var module = angular.module('controllers');

    module.controller('nodeController', ["$rootScope", "$scope", "$location", "nodeService", function($rootScope, $scope, $location, nodeService) {
        $scope.keywordItems = [];
        $scope.isNodePage = $location.search().create ||  $location.search().update ? true : false;
        $scope.isNodeCreation = $location.search().create ? true : false;
        $scope.totalPages = 0;
        $scope.currentPage = 0;

        if($scope.isNodeCreation) {
            $rootScope.newNode = {};
        }

        $scope.updateCurrentNode = function(node) {
            $rootScope.newNode = node;
        };

        $scope.deleteCurrentNode = function(node) {
            nodeService.delete(node)
                .then(function() {
                    $scope.nodes.forEach(function(nodeInt,index) {
                        if (nodeInt.id === node.id) {
                            $scope.nodes.splice(index,1);
                        }
                    });
                });
        };

        $scope.getAllCategories = function() {
            nodeService.getAllCategories()
                .then(function (data) {
                    $scope.categories = data;
                });
        };

        $scope.getAllNodes = function (page) {
            nodeService.getAllNodes(page)
                .then(function(data) {
                    $scope.nodes = data.content;
                    $scope.totalPages = new Array(data.totalPages);
                    $scope.currentPage = data.number;
                });
        };

        $scope.addNode = function() {
            if($scope.newNode.keywords) {
                $scope.newNode.keywords = transformObjectToArray($scope.newNode.keywords);
            } else if($scope.newNode.facets) {
                for(var key in $scope.newNode.facets) {
                    $scope.newNode.facets[key] = transformObjectToArray($scope.newNode.facets[key]);
                }
            }
            if(!$scope.isNodeCreation) {
                nodeService.update($scope.newNode)
                    .then(function (data) {
                        $rootScope.newNode = {};
                    });
            } else {
                nodeService.add($scope.newNode)
                    .then(function (data) {
                        $scope.newNode = {};
                    });
            }

            function transformObjectToArray(object) {
                var array = [];
                for(var key in object) {
                    array.push(object[key]);
                }

                return array;
            }
        };

        $scope.addKeywordItem = function(){
            if($scope.isNodeCreation) {
                $scope.keywordItems.push("");
            } else {
                $scope.newNode.keywords.push("");
            }
        };

        $scope.removeKeywordItem = function(){
            if($scope.isNodeCreation) {
                $scope.keywordItems.pop();
            } else {
                $scope.newNode.keywords.pop();
            }
        };
    }]);
})();