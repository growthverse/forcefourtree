(function () {
	"use strict";

	var application = angular.module('forcefourtree', [
		'ng',
		'ngRoute',
		'restangular',
		'controllers',
		'services',
		'login',
		'graph',
		'tree'
	]);

	application.config(['$routeProvider', function ($routeProvider) {
		$routeProvider
		.when('/', {
			controller: 'indexController',
			templateUrl: '/static/app/views/index.html'
		})
		.when("/node", {
			templateUrl: "/static/app/views/node.html",
			controller: "nodeController"
		})
		.when("/user", {
			templateUrl: "/static/app/views/user.html",
			controller: "userController"
		})
	}]);
})();