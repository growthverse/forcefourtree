/**
 * Fichier non utilise car non termine de developper => NE MARCHE PAS
 */
(function () {
    "use strict";

    var application = angular.module('graph');

    application.directive('graph', ['$window', function($window) {
        return {
            restrict: 'E',
            replace: true,
            transclude: false,
            scope: false,
            templateUrl: '/static/app/directives/graph/graph.html',
            controller: 'graphController',
            link: function (scope, element) {
                var width = 1000;
                var height = 500;
                var color = d3.scale.category20();

                scope.$watch('grapheDatas', function (grapheDatas) {
                    var force = d3.layout.force()
                        .charge(-120)
                        .linkDistance(30)
                        .size([width, height])
                        .nodes(grapheDatas.nodes)
                        .links(grapheDatas.links)
                        .start();

                     var svg = d3.select("#graph").append("svg")
                         .attr("width", width)
                         .attr("height", height);

                     var link = svg.selectAll(".link")
                         .data(grapheDatas.links)
                         .enter().append("line")
                         .attr("class", "link")
                         .style("stroke-width", function(d) { return Math.sqrt(d.value); });

                     var node = svg.selectAll(".node")
                         .data(grapheDatas.nodes)
                         .enter().append("circle")
                         .attr("class", "node")
                         .attr("r", 5)
                         .style("fill", function(d) { return color(d.group); })
                         .call(force.drag)
                         .on("click", click);
                    function click()
                    {
                        scope.majChoix(d.name)
                    }

                     node.append("title")
                         .text(function(d) { return d.name; });

                     force.on("tick", function() {
                         link.attr("x1", function(d) { return d.source.x; })
                             .attr("y1", function(d) { return d.source.y; })
                             .attr("x2", function(d) { return d.target.x; })
                             .attr("y2", function(d) { return d.target.y; });

                         node.attr("cx", function(d) { return d.x; })
                             .attr("cy", function(d) { return d.y; });
                    });
                });
            }
        };
    }]);
})();
