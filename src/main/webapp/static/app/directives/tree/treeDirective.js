(function () {
    "use strict";

    var application = angular.module('tree');

    application.directive('tree', ['$window', 'treeService', function($window, treeService) {
        return {
            restrict: 'E',
            replace: true,
            transclude: false,
            scope: false,
            templateUrl: '/static/app/directives/tree/tree.html',
            controller: 'treeController',
            link: function (scope, element) {
                scope.breadcrumb = [];
                var currentTree;

                uploadTree();

                function createTree(tree) {
                    var width = $window.innerWidth / 2;
                    var height = $window.innerHeight - 150;
                    var color = d3.scale.category10();

                    var cluster = d3.layout.cluster()
                        .size([height, width - 200]);

                    var diagonal = d3.svg.diagonal()
                        .projection(function(d) { return [d.y, d.x]; });

                    var svg = d3.select("#tree").append("svg")
                        .attr("width", width)
                        .attr("height", height)
                        .append("g")
                        .attr("transform", "translate(40,0)");

                    var nodes = cluster.nodes(tree),
                        links = cluster.links(nodes);

                    var link = svg.selectAll(".link")
                        .data(links)
                        .enter().append("path")
                        .attr("class", "link")
                        .attr("d", diagonal);

                    var node = svg.selectAll(".node")
                        .data(nodes)
                        .enter().append("g")
                        .attr("id", function(d) {
                            return d.name;
                        })
                        .attr("class", "node")
                        .attr("transform", function(d) {
                            return "translate(" + d.y + "," + d.x + ")";
                        });

                    node.append("circle")
                        .attr("r", 15)
                        .attr("class", "facet")
                        .style("fill", function(d) { return color(d.name); })
                        .on("click", function(d) {
                            if(!d.hasOwnProperty("root") && d.type !== "leaf") {
                                uploadTree(d.name);
                            }
                        })
                        .on("mouseover", function(d) {
                            _.forEach((d.leaves), function(leaf) {
                                d3.select("#" + d.name + '_' + leaf.name.split(' ').join('_'))
                                    .attr("display", "initial");

                                d3.selectAll("." + d.name)
                                    .attr("opacity", 0.1);
                            });

                        })
                        .on("mouseout", function(d) {
                            _.forEach((d.leaves), function(leaf) {
                                d3.select("#" + d.name + '_' + leaf.name.split(' ').join('_'))
                                    .attr("display", "none");

                                d3.selectAll("." + d.name)
                                    .attr("opacity", 1);
                            });

                        });

                    node.append("text")
                        .attr("class", function(d) { return d.name })
                        .attr("dx", function(d) { return d.children ? 40 : 20; })
                        .attr("dy", function(d) { return d.children ? 30 : 20; })
                        .attr("stroke-width", 0.5)
                        .attr("stroke", "black")
                        .style("text-anchor", function(d) { return d.children ? "end" : "start"; })
                        .text(function(d) { return d.name; });

                    nodes.forEach(function(node) {
                        if(node.leaves) {
                            var n = d3.select("#" + node.name);
                            var round = 2*Math.PI/node.leaves.length;
                            for (var i = 0; i < node.leaves.length; i++) {
                                var x = 40 * Math.sin(i * round);
                                var y = -(40 * Math.cos(i * round));
                                n.append("circle").attr({
                                    class: "leaf",
                                    cx: x,
                                    cy: y,
                                    opacity: 1,
                                    r: 5
                                }).style("fill", color(node.name));

                                n.append("text")
                                    .attr("dx", function(d) {
                                        return x < 0 ? x - 7 : x + 7;
                                    })
                                    .attr("dy", y + 2)
                                    .attr("id", (node.name + '_' + node.leaves[i].name.split(' ').join('_')))
                                    .attr("class", "label_leaf")
                                    .attr("display", "none")
                                    .attr("stroke-width", 0.1)
                                    .attr("stroke", "black")
                                    .attr("text-anchor", function(d) {
                                       return x < 0 ? "end" : "start";
                                    })
                                    .text(node.leaves[i].name);
                            }
                        }
                    });

                    function updateWindow(){
                        d3.select("svg").remove();
                        if(currentTree) {
                            createTree(currentTree);
                        }
                    }
                    window.onresize = updateWindow;
                }

                function uploadTree(node) {
                    var params = "";
                    if(node) {
                        // Manage history
                        addNodeToBreadcrumb(node);

                        // Get string params for request
                        params = convertBreadcrumbToString();
                    }

                    treeService.get(params)
                        .then(function(data) {
                            var tree = {
                                name: data.name,
                                root: true,
                                children: data.outlinks
                            };
                            currentTree = tree;
                            d3.select("svg").remove();
                            createTree(tree);
                        });
                }

                function addNodeToBreadcrumb(node) {
                    if(scope.breadcrumb.length === 0 || (scope.breadcrumb.length > 0 && scope.breadcrumb[scope.breadcrumb.length - 1].keyword !== '')) {
                        var object = {
                            category: node,
                            keyword: ''
                        };
                        scope.breadcrumb.push(object);
                    } else {
                        scope.breadcrumb[scope.breadcrumb.length - 1].keyword = node;
                    }
                }

                function convertBreadcrumbToString() {
                    var params = "";
                    var index = 0;

                    if(scope.breadcrumb.length > 0) {
                        while (index < scope.breadcrumb.length - 1) {
                            var object = scope.breadcrumb[index];
                            if (object.keyword !== "") {
                                params += object.category + "=" + object.keyword + "&";
                            } else {
                                params += object.category;
                                break;
                            }
                            index++;
                        }

                        if (index === scope.breadcrumb.length - 1) {
                            var object = scope.breadcrumb[index];
                            if (object.keyword !== "") {
                                params += object.category + "=" + object.keyword;
                            } else {
                                params += object.category;
                            }
                        }
                    }

                    return params;
                }

                scope.updateBreadcrumb = function(node, type) {
                    var index = _.findIndex(scope.breadcrumb, function(object) {
                        return object[type] === node;
                    });
                    if(type === "keyword") {
                        scope.breadcrumb[index].keyword = "";
                        index += 1;
                    }
                    scope.breadcrumb = _.slice(scope.breadcrumb, 0, index);
                    uploadTree(node);
                }
            }
        };
    }]);
})();