<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!Doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="forcefourtree" ng-strict-di>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>ForceFourTree</title>

	<!-- Style -->
	<link rel="stylesheet" href="/static/bower_components/bootstrap/dist/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/static/css/general.css" />
	<link rel="stylesheet" href="/static/css/node.css" />
	<link rel="stylesheet" href="/static/css/graph.css" />
	<link rel="stylesheet" href="/static/css/tree.css" />

	<!-- Libs -->
	<script src="/static/bower_components/jquery/dist/jquery.min.js" defer></script>
	<script src="/static/bower_components/angular/angular.js" defer></script>
	<script src="/static/bower_components/angular-route/angular-route.min.js" defer></script>
	<script src="/static/bower_components/bootstrap/dist/js/bootstrap.min.js" defer></script>
	<script src="/static/bower_components/lodash/lodash.min.js" defer></script>
	<script src="/static/bower_components/restangular/dist/restangular.min.js" defer></script>
	<script src="/static/bower_components/d3/d3.min.js" defer></script>

	<!-- JS -->
	<script src="/static/app/app.js" defer></script>

	<!-- Controllers -->
	<script src="/static/app/controllers/module.js" defer></script>
	<script src="/static/app/controllers/indexController.js" defer></script>
	<script src="/static/app/controllers/graphController.js" defer></script>
	<script src="/static/app/controllers/treeController.js" defer></script>
	<script src="/static/app/controllers/nodeController.js" defer></script>
	<script src="/static/app/controllers/menuController.js" defer></script>
	<script src="/static/app/controllers/userController.js" defer></script>

	<!-- Services -->
	<script src="/static/app/services/module.js" defer></script>
	<script src="/static/app/services/nodeService.js" defer></script>
	<script src="/static/app/services/userService.js" defer></script>

	<!-- Directives -->
	<script src="/static/app/directives/login/module.js" defer></script>
	<script src="/static/app/directives/login/loginController.js" defer></script>
	<script src="/static/app/directives/login/loginDirective.js" defer></script>
	<script src="/static/app/directives/graph/module.js" defer></script>
	<script src="/static/app/directives/graph/graphController.js" defer></script>
	<script src="/static/app/directives/graph/graphDirective.js" defer></script>
	<script src="/static/app/directives/tree/module.js" defer></script>
	<script src="/static/app/directives/tree/treeController.js" defer></script>
	<script src="/static/app/directives/tree/treeDirective.js" defer></script>
	<script src="/static/app/directives/tree/treeService.js" defer></script>

</head>
	<body>
		<ng-include src="'/static/app/views/menu.html'"></ng-include>
		<div class="container">
			<main ng-view>
			</main>
		</div>
	</body>
</html>




