package com.univ.smb.forceFourTree.config;

import com.univ.smb.forceFourTree.authentication.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomAuthenticationProvider customAuthenticationProvider;

    @Autowired
    AuthFailureHandler authFailureHandler;

    @Autowired
    AuthSuccessHandler authSuccessHandler;

    @Autowired
    CustomEntryPoint customEntryPoint;

    @Autowired
    HttpLogoutSuccessHandler httpLogoutSuccessHandler;

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(customAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.exceptionHandling().authenticationEntryPoint(customEntryPoint);
        http.authorizeRequests()
                .antMatchers("/node/create.json").access("hasRole('ROLE_COMMITTER') || hasRole('ROLE_MODERATOR') || hasRole('ROLE_ADMIN')")
                .antMatchers("/node/update/**").access("hasRole('ROLE_ADMIN') || hasRole('ROLE_MODERATOR')")
                .antMatchers("/node/delete/**").access("hasRole('ROLE_ADMIN') || hasRole('ROLE_MODERATOR')")
                .antMatchers("/user/update/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/user/delete/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/user/getAllUsers/**").access("hasRole('ROLE_ADMIN')")
                .and()
                .formLogin()
                .failureHandler(authFailureHandler)
                .successHandler(authSuccessHandler)
                .usernameParameter("name").passwordParameter("password")
                .and()
                .logout()
                .invalidateHttpSession(true)
                .logoutSuccessHandler(httpLogoutSuccessHandler)
        ;

    }

}