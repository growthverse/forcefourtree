package com.univ.smb.forceFourTree.controllers.rest;

import com.univ.smb.forceFourTree.models.Node;
import com.univ.smb.forceFourTree.models.Outlink;
import com.univ.smb.forceFourTree.models.Tree;
import com.univ.smb.forceFourTree.repositories.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("tree")
public class TreeController {

    @Autowired
    NodeRepository nodeRepository;

    @RequestMapping(
            value = "filter",
            method = RequestMethod.GET
    )
    @ResponseBody
    public Tree filter(
            HttpServletRequest request
    ) {
        Tree tree_returned = null;

        if(!request.getParameterMap().isEmpty()) {
            Map<String, String[]> parameters = request.getParameterMap();

            // Compute nodes filtering by facets
            Map<String, String[]> facets = new HashMap<String, String[]>();
            Node category = null; // We store the last category selected
            String lastKeyword = null; // We store the last keyword selected
            String[] keywords = null;

            if (!parameters.isEmpty()) {
                for (String key : parameters.keySet()) {
                    category = nodeRepository.findByName(key);
                    if (category != null) {
                        keywords = parameters.get(key);
                        // If the current category is filtered by a keyword
                        if (keywords.length > 1 || !StringUtils.isEmpty(keywords[0])) {
                            facets.put(category.getId(), keywords);
                            lastKeyword = keywords[keywords.length - 1];
                        } else {
                            facets.put(category.getId(), null);
                        }
                    }
                }

                List<Node> nodes = new ArrayList<>();
                if (!facets.isEmpty()) {
                    nodes = nodeRepository.filterByFacets(facets);
                }
            /*
             * Format response for frontend
             */

                if (category != null) {

                    // Tree json for frontend
                    Tree tree = new Tree();

                    // Retrieve the category filters
                    keywords = facets.get(category.getId());

                    // Last parameter is a single category
                    if (keywords == null) {
                        // Node clicked is a category
                        tree.setName(category.getName());

                        List<String> categoryKeywords = category.getKeywords();
                        if (categoryKeywords != null) {
                            for (String keyword : categoryKeywords) {
                                List<Node> leaves = new ArrayList<Node>();
                                for (Node leaf : nodes) {
                                    Map<String, List<String>> nodeFacets = leaf.getFacets();
                                    if (nodeFacets.containsKey(category.getId())
                                            && nodeFacets.get(category.getId()).contains(keyword)) {
                                        leaves.add(leaf);
                                    }
                                }
                                Outlink outlink = new Outlink();
                                outlink.setName(keyword);
                                outlink.setLeaves(leaves);
                                tree.addOutlink(outlink);
                            }
                        }
                    }
                    // Last parameter is a category filter by keywords
                    else {
                        // Node clicked is a keyword
                        tree.setName(lastKeyword);

                        List<Node> categories = nodeRepository.findByType("category");
                        // There is no more category
                        if (facets.size() == categories.size()) {
                            List leaves = new ArrayList<>();
                            for (Node leaf : nodes) {
                                leaves.add(leaf);
                            }
                            tree.setOutlinks(leaves);
                        }
                        // There is remaining categories to filter again
                        else {
                            List<Node> remainingCategories = new ArrayList<Node>();
                            Set<String> selectedCategories = facets.keySet();
                            for (Node nodeCategory : categories) {
                                if (!selectedCategories.contains(nodeCategory.getId())) {
                                    remainingCategories.add(nodeCategory);
                                }
                            }
                            for (Node remainingCategory : remainingCategories) {
                                List<Node> leaves = new ArrayList<Node>();
                                for (Node leaf : nodes) {
                                    Map<String, List<String>> nodeFacets = leaf.getFacets();
                                    if (nodeFacets.containsKey(remainingCategory.getId())) {
                                        leaves.add(leaf);
                                    }
                                }
                                Outlink outlink = new Outlink();
                                outlink.setName(remainingCategory.getName());
                                outlink.setLeaves(leaves);
                                tree.addOutlink(outlink);
                            }
                        }
                    }

                    tree_returned = tree;
                }
            }
        } else {
            tree_returned = new Tree();
            tree_returned.setName("Categories");
            List<Node> categories = nodeRepository.findByType("category");
            for (Node category : categories) {
                HashMap<String, String[]> facetsCategory = new HashMap<>();
                facetsCategory.put(category.getId(), null);
                Outlink outlink = new Outlink();
                outlink.setName(category.getName());
                outlink.setLeaves(nodeRepository.filterByFacets(facetsCategory));
                tree_returned.addOutlink(outlink);
            }
        }

        return tree_returned;
    }
}
