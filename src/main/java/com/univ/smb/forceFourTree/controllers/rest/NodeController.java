package com.univ.smb.forceFourTree.controllers.rest;

import com.univ.smb.forceFourTree.models.Node;
import com.univ.smb.forceFourTree.repositories.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("node")
public class NodeController {

    @Autowired
    NodeRepository nodeRepository;

    @RequestMapping(
            value = "/create",
            headers = "Accept=*/*",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Node post(HttpServletRequest request, @RequestBody Node node) {
        Node newNode = nodeRepository.save(node);
        return newNode;
    }

    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.GET
    )
    public Node get(HttpServletRequest request, @PathVariable String id) {
        Node node = nodeRepository.findOne(id);
        return node;
    }

    @RequestMapping(
            value = "/update/{id}",
            headers = "Accept=*/*",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Node update(
            HttpServletRequest request,
            @PathVariable String id,
            @RequestBody Node newNode
    ) {
        Node node = nodeRepository.findOne(id);
        if(node != null) {
            node.setName(newNode.getName());
            node.setType(newNode.getType());
            node.setFacets(newNode.getFacets());
            node.setKeywords(newNode.getKeywords());
            nodeRepository.save(node);
        }
        return node;
    }

    @RequestMapping(
            value = "/delete/{id}",
            headers = "Accept=*/*",
            method = RequestMethod.DELETE
    )
    public void delete(
            HttpServletRequest request,
            @PathVariable String id
    ) {
        Node node = nodeRepository.findOne(id);
        if(node != null) {
            nodeRepository.delete(node);
        }
    }

    @RequestMapping(
            value = "/getAllCategories",
            method = RequestMethod.GET
    )
    public List<Node> getAllCategories(
            HttpServletRequest request
    ) {
        List<Node> categories = nodeRepository.findByType("category");
        return categories;
    }

    @RequestMapping(
            value = "/getAllNodes",
            method = RequestMethod.GET
    )
    public Page<Node> getAllNodes(
            HttpServletRequest request,
            Pageable pageRequest
    ) {
        Page<Node> nodes = nodeRepository.findAll(pageRequest);
        return nodes;
    }
}
