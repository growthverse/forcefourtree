package com.univ.smb.forceFourTree.controllers.app;

import com.univ.smb.forceFourTree.models.Node;
import com.univ.smb.forceFourTree.models.NodeList;
import com.univ.smb.forceFourTree.repositories.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    NodeRepository nodeRepository;

    @RequestMapping(
            value = {"/", "/index"},
            method = RequestMethod.GET
    )
    public ModelAndView index(HttpServletRequest request) {
        List<Node> nodes = nodeRepository.findAll();
        NodeList nodeList = new NodeList(nodes);
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("nodes", nodeList);
        return modelAndView;
    }

}
