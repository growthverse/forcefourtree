package com.univ.smb.forceFourTree.controllers.rest;

import com.univ.smb.forceFourTree.bean.SessionBean;
import com.univ.smb.forceFourTree.models.Node;
import com.univ.smb.forceFourTree.models.User;
import com.univ.smb.forceFourTree.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private SessionBean sessionBean;

    @RequestMapping(
            value = "/create",
            headers = "Accept=*/*",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public User post(HttpServletRequest request, @RequestBody User user) {
        User newUser = userRepository.save(user);
        return newUser;
    }

    @RequestMapping(
            value = "/update/{id}",
            headers = "Accept=*/*",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public User update(
            HttpServletRequest request,
            @PathVariable String id,
            @RequestBody User newUser,
            HttpServletResponse response
    ) {

        User user = userRepository.findOne(id);
        if(user != null) {
            user.setRole(newUser.getRole());
            userRepository.save(user);
        }
        return user;
    }

    @RequestMapping(
            value = "/delete/{id}",
            headers = "Accept=*/*",
            method = RequestMethod.DELETE
    )
    public void delete(
            HttpServletRequest request,
            @PathVariable String id
    ) {
        User user = userRepository.findOne(id);
        if(user != null) {
            userRepository.delete(user);
        }
    }
    
    @RequestMapping(
            value = "/getAllUsers",
            method = RequestMethod.GET
    )
    public Page<User> getAllUsers(
            HttpServletRequest request,
            Pageable pageRequest
    ) {
        Page<User> Users = userRepository.findAll(pageRequest);
        return Users;
    }


}
