package com.univ.smb.forceFourTree.bean;

import com.univ.smb.forceFourTree.models.User;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionBean implements Serializable {

    private User user = new User();

    public boolean allowCommitter() {
        return (user.getRole().equals("committer"));
    }

    public boolean allowModerator() {
        return (user.getRole().equals("moderator"));
    }

    public boolean allowAdmin() {
        return (user.getRole().equals("admin"));
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
