package com.univ.smb.forceFourTree.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name="nodes")
public class NodeList {

    private List<Node> nodeList;

    public NodeList() {}
    public NodeList(List<Node> nodeList) {
        this.nodeList = nodeList;
    }

    @XmlElement(name="node")
    public List<Node> getNodeList() {
        return nodeList;
    }
    public void setNodeList(List<Node> nodeList) {
        this.nodeList = nodeList;
    }

}
