package com.univ.smb.forceFourTree.authentication;

import com.univ.smb.forceFourTree.bean.SessionBean;
import com.univ.smb.forceFourTree.models.User;
import com.univ.smb.forceFourTree.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private SessionBean sessionBean;

    /**
     * {@inheritDoc}
     */
    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {

        System.out.println("authentication.name = " + authentication.getName());
        System.out.println("authentication.password = " + authentication.getCredentials().toString());
        final String name = authentication.getName();
        final String password = authentication.getCredentials().toString();

        User newUser = userRepository.findByName(name);
        sessionBean.setUser(newUser);
        if(newUser == null || !(newUser.getPassword().equals(password))) {
            System.out.println("=== authentication fail");
            newUser.setLoginError(true);
            throw new BadCredentialsException("Bad Login");
        }

        String role = "ROLE_" + newUser.getRole().toUpperCase();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);

        Collection<SimpleGrantedAuthority> simpleGrantedAuthorityCollection = new ArrayList<>();
        simpleGrantedAuthorityCollection.add(authority);

        if (role.equals("ROLE_ADMIN") || role.equals("ROLE_MODERATOR")) {
            role = "ROLE_MODERATOR";
            SimpleGrantedAuthority authority2 = new SimpleGrantedAuthority(role);
            simpleGrantedAuthorityCollection.add(authority2);
        }

        if (role.equals("ROLE_COMMITTER")) {
            SimpleGrantedAuthority authority3 = new SimpleGrantedAuthority(role);
            simpleGrantedAuthorityCollection.add(authority3);
        }

        // Pour finir on retourne un objet de type Authentication
        return new UsernamePasswordAuthenticationToken(name, password, simpleGrantedAuthorityCollection);

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
