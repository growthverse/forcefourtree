package com.univ.smb.forceFourTree.controllers;

import com.univ.smb.forceFourTree.controllers.rest.TreeController;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
@Ignore
public class JsonControllerTest {

    @InjectMocks
    private TreeController treeController;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(treeController).build();
    }


    @Test
    public void withNoParametersShouldGetTreeEmpty() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tree/filter.json")
                .header("Content-Type", "application/json")
                .header("Accept", "*/*"))
                .andExpect(status().isOk());
    }

    @Test
    public void withParametersBadCategoryShouldGetTreeEmpty() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tree/filter.json")
                .header("Content-Type", "application/json")
                .header("Accept", "*/*")
                .pathInfo("/tree/filter.json?Test"))
                .andExpect(status().isOk());
    }

    @Test
    public void withParametersCategoryAndKeywordsShouldGetTreeNotEmpty() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tree/filter.json")
                .header("Content-Type", "application/json")
                .header("Accept", "*/*")
                .pathInfo("/tree/filter.json?Genre=Action&Public=Adulte"))
                .andExpect(status().isOk());

                /*
                .andExpect(jsonPath("$.name", is("Adulte")))
                .andExpect(jsonPath("$.outlinks", hasSize(1)))
                .andExpect(jsonPath("$.outlinks[0].id", is("5620166dd4c6c1ae413343fe")))
                .andExpect(jsonPath("$.outlinks[0].name", is("Mission Impossible")))
                .andExpect(jsonPath("$.outlinks[0].type", is("leaf")))
                .andExpect(jsonPath("$.outlinks[0].facets", hasSize(2)))
                .andExpect(jsonPath("$.outlinks[0].facets[0]", hasSize(2)));*/
    }
}
