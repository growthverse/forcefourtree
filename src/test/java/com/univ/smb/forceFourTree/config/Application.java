package com.univ.smb.forceFourTree.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.univ.smb.forceFourTree.**")
public class Application extends SpringBootServletInitializer {

}
